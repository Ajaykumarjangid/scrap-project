from django.shortcuts import render
from django.http import HttpResponse
from bs4 import BeautifulSoup
import requests
from rest_framework import generics
from rest_framework.response import Response

dic = {}
def India(q):
    name = q
    url = 'https://en.wikipedia.org/wiki/{}'.format(name)
    header = {
        "User-Agent": 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36'}
    html = requests.get(url, headers=header)
    soup = BeautifulSoup(html.content, "html.parser")
    Div = soup.find_all("table", {"class": "infobox ib-country vcard"})[0]
    try:
        dic ['flag_link'] = Div.find('img')['src']  
    except:
        dic ['flag_link'] = " "
    try:
        #dic['capital'] =Div.find_all('td',{'class':'infobox-data'})[0].a.text
        for tag in Div.find_all("tr"):
            for i in tag:
                if(i.string == "Capital"):
                    if i.next_sibling.find('div') == None:
                        dic['capital'] =[i.next_sibling.a.text ]
                    else:
                        dic['capital'] =[x.a.text for x in i.next_sibling.div.find_all('li') ]
    except:
        dic['capital'] =""
    try:
        #dic['largest_city'] = [x.a.text for x in Div.find_all('ul')[0].find_all('li')]
         for tag in Div.find_all("tr"):
            for i in tag:
                if(i.string == "Largest city"):
                    if i.next_sibling.find('div') == None:
                        dic['largest_city'] =[i.next_sibling.a.text ]
                    else:
                        dic['largest_city'] =[x.a.text for x in i.next_sibling.div.find_all('li') ]
      
    except:
        dic['largest_city'] = ""
    try:
        for tag in Div.find_all("tr"):
            for i in tag:
                if(i.string == "Official languages"):
                    if i.next_sibling.find('div') == None:
                        dic['official_languages'] =[i.next_sibling.a.text ]
                    else:
                        dic['official_languages'] =[x.a.text for x in i.next_sibling.div.find_all('li') ]
    except:
        dic['official_languages'] = ""
    try:
        for tag in Div.find_all("tr"):
            for i in tag:
                if(i.string == "Area "):
                    dic['area_total'] =i.parent.next_sibling.td.text.split(' ')[0]
    except:
        dic['area_total'] = ""
    try:
        for tag in Div.find_all("tr"):
            for i in tag:
                if(i.string == "Population"):
                    dic['population'] =i.parent.next_sibling.td.text.split('[')[0]
    except:
        dic['population'] =""
    try:
        for tag in Div.find_all("a",{'title':'Gross domestic product'})[1:]:
            for i in tag:
                if(i.string == "GDP"):
                    dic['gdp_nominal'] =i.parent.parent.parent.next_sibling.td.text
        

    except:
        dic['gdp_nominal'] =""
    
    return dic



    
    
class Demo(generics.ListAPIView):
    def list(self, request,name):
        print(name,'==========43')
        data=India(name)
        
        message = ("data featch  sucessfully")
        return Response({'status': True, 'message': message, 'data': data, })
